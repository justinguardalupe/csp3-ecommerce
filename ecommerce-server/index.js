const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const orderRoutes = require("./routes/order");
const productRoutes = require("./routes/product");

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//to connect to mongoDB Atlas
mongoose.connect("mongodb+srv://dbjustinguardalupe:SBQlxjwdpK9JMapi@wdc028-course-booking.gqwer.mongodb.net/csp3ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}	
);

//to check if we are connected to MongoDB

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Cloud database connection error"));
db.once("open", () => console.log(`Connected to MongoDB Atlas cloud`));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Middleware

app.use((req, res, next) => {
	console.log(`request method: ${req.method}`);
	console.log(`request headers: ${req.headers.authorization}`);
	console.log(`request body: ${req.body}`);
	next();
})

app.use((err, req, res, next) => {
	console.log(err)
	res.status(err.status).json({
		message: err.message })
})	

//environment variable port or port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API on Port ${ process.env.PORT || 4000 } is now online`)
});
