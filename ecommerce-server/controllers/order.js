const Order = require("../models/Order");
const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');

// Create new order *User only
module.exports.createOrder = async (userData, reqBody) => {
	if(!userData.isAdmin){
		let userID = await User.findOne({_id: userData.id})
		.then(result => {
			return result.id;
		});

		let productID = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.id;
		});

		let price = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.price;
		});

		let productQuantity = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.stocks;
		});

		let productName = await Product.findOne({_id: reqBody.productId})
		.then(result => {
			return result.name;
		});

		let myOrder = new Order({
			userId: userID,
			productId : productID,
			orderQuantity: reqBody.quantity,
			totalAmount : reqBody.quantity * price
		})

		Product.findOne({_id: reqBody.productId})
		.then(result => {
			if(result.stocks >= reqBody.quantity) {
				let productSummary = {
					orderedOn : myOrder.orderedOn,
					orderId : myOrder.id,
					orderQuantity : reqBody.quantity
				}

				result.stocks = result.stocks - reqBody.quantity;
				result.orderSummary.push(productSummary);

				return result.save()
				.then((result, error) => {
					if(error) {
						return `Updating product failed.`;
					}
					else {
						return `Successfully updated the product.`;
					}
				})
			}
			else {
				return `Insufficient supply of product.`;
			}
		})

		User.findOne({_id: userData.id})
		.then(result => {
			if(productQuantity >= myOrder.quantity){
				let orderSummary = {
					orderedOn : myOrder.orderedOn,
					orderId : myOrder.id,
					productId : myOrder.productId,
					quantity : myOrder.quantity,
					totalAmount : myOrder.totalAmount
				}

				result.orders.push(orderSummary);

				return result.save()
				.then((result, error) => {
					if(error) {
						return `Updating user failed.`;
					} else {
						return `Successfully updated the user.`;
					}
				})
			} else {
				return `Insufficient supply of product.`;
			}
		})

		if(productQuantity >= reqBody.quantity) {
			return myOrder.save()
			.then((order, error) => {
				if(error) {
					return `The new order request failed.`;
				} else {
					return `Successfully created the order.`;
				}
			})
		}
		else {
			return `Insufficient supply of product.`;
		}

	} else {
		return `You don't have access to these commands.`;
	}
}

// Retrieve all orders *Admin only
module.exports.getAllOrders = async (userData) => {
	if(userData.isAdmin){
		return Order.find().then(result => {
			return result;
		})
	} else {
		return `You don't have access to these commands.`
	}
}


// Retrieve user orders *User only
module.exports.getMyOrders = async (userData) => {
	if(!userData.isAdmin){
		return Order.findOne({userId:userData.id}).then(result => {
			if (result == null){
        return false
      }
        else{
          return result;
        }
      })
    }
    else{
      return false
   }
}
