const Product = require("../models/Product");
const auth = require("../auth");

// Create product * ADMIN ONLY
module.exports.createProduct = async (reqBody, userData) => {
	if(userData.isAdmin){
		let newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stocks : reqBody.stocks
		})
		return newProduct.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return `You don't have access to this command.`
	}
	
}


// retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({isActive : true}).then(result => {
		if(result == null || result == undefined) {
	    return "Product does not exist."
	  }
	  else {
      return result;
    }
	})
}

// retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findOne({_id : reqParams.productId}).then(result => {
		return result;
	}).catch((error) => {
		return `No known product.`
	})
}

// update a product * ADMIN ONLY
module.exports.updateProduct = (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		let updateProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stocks : reqBody.stocks
		}
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
		.then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return "You don't have access";
	}
}

// archive a product *ADMIN ONLY
module.exports.archiveProduct = async (userData, reqParams) => {
	if(userData.isAdmin){
		return Product.findByIdAndUpdate({_id : reqParams.productId}, {isActive: false}).then(result => {
			return result.save().then((updatedResult, error) => {
				if(error){
					return `Error: Archiving failed.`
				} else {
					return `Archiving successful. ${updatedResult}`;
				}
			});
		})
	}
	else{
		return `You don't have access.`
	}
}
