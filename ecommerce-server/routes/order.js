const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

// Route for creating an order
router.post('/users/checkout', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.createOrder(userData, req.body).then(result => res.send(result));
})

// Route for retrieving all orders
router.get('/users/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getAllOrders(userData).then(result => res.send(result));
})

// Retrieve Authenticated User's Orders
router.get('/users/myOrders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getMyOrders(userData, req.body).then(result => res.send(result));
})

module.exports = router;